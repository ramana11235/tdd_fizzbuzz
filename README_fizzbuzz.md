# Testing / System Testing / TDD

This repo is going to be a TDD exercise for fizzbuzz with a focufocus on testing and system testing.

## Testing & System Testing

The importance of testing:
- Lets us (& client) know when we are done
- Reduces technical debt
- Code becomes more maintainable 
- Allows the system to be checked as they are changed for updates.
- Important for compliance (usually external testing teams)

The Different Teams on a Product:
- Dev Team
- DevOps (Ops Team)
- Testers
- Business / Product Team

## Different types of testing
- Black Box Testing
    - Don't know/Don't care how the code is running
    - For testing the team as a whole
    - Tools: Selenium - Opens up a web browser and clicks and writes stuff
- White box (transluscent) testing 
    - Aware of the underbelly of the code
    - Test individual sections
    - More like unit testing

## Levels of testing
- Unit testing (single functions and classes/methods)
- Integration testing (groups of functions and classes/methods together)
- System testing (functions and classes working on a system (a server in port 80))
- Acceptance testing (human who is paying saying its good)

## Importance of testing for DevOps and Agile 

- In terms of Agile, it ensures working code.
- In terms of DevOps, code deployement is automated, so it's crucial that the mistakes aren't automated.
- Mistakes are made normally, it's important that costly mistakes are caught in testing.


## FizzBuzz Exercise
- FizzBuzz classic exercise in programming community
- We'll create this using User Stories, Acceptance Criteria and Definition of Done (DoD)

## Difference between acceptance criteria and Definition of Done 
- Acceptance Criteria --> is unique to one card/user story
- Definition of Done --> applies to EVERY card/user story (meta work)
  - e.g. Every user story should be a separate branch on git 
  - Every user story should be documented 
  - Once the user story in review, need to be emailed to mike
  - Things agreed upon as a team 

### Main user stories and Epics

- [Epic]
  - As a user, I want a program that runs FizzrBuzz, up to any number which should output a list and should be tested so I know it works
- [User Story]
  - As a user, when the program gets a multiple of 3 it should print out fizz 
- [User Story]
  - As a user, when the program gets a multiple of 5 it should print out buzz
  
- [User Story]
  - As a user, when the program gets a multiple of 3 and 5 it should print out fizzbuzz
  
- [User Story]
  - As a user, when I give the program a number, it should play FizzBuzz upto that number.